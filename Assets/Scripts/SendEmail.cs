﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

/**
 * @author Anthony Wong
 * @class This class is dedicated to send email.  
 */ 
public class SendEmail : MonoBehaviour {

	/**
	 * The SMTP Email Address
	 */ 
	private const string EMAIL = "mine.sweeper1412@gmail.com";

	/**
	 * The SMTP Email Password
	 */ 
	private const string PASSWORD = "mine.sweeper";

	/**
	 * Send an email to a recipient
	 * @email The recipient's email
	 * @subject The subject of the email
	 * @message The message of the email
	 */ 
	public static void sendEmail(string email, string subject , string message){
		MailMessage mail = new MailMessage();
		mail.From = new MailAddress(EMAIL);
		mail.To.Add(email);
		mail.Subject = subject;
		mail.Body = message;
		
		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential(EMAIL, PASSWORD);
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
		{ return true; };
		smtpServer.Send(mail);
	}	
}