﻿using UnityEngine;
using System.Collections;

/**
 * @author Anthony Wong
 * @class This class is dedicated to change scene.
 */ 
public class ChangeScene : MonoBehaviour {

	/**
	 * This change the current scene
	 * to another scene.
	 * @scene Change the current Scene with the given scene.
	 */ 
	public void ChangeTo(string scene){
		Application.LoadLevel(scene);
	}

	/**
	 * This change the current scene to another scene.
	 * @scene Change the current scene with the given scene.
	 */ 
	public static void ChangeToStatic(string scene){
		Application.LoadLevel(scene);
	}
	

}
