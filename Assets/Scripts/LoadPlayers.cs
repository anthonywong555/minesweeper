using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * @author Anthony Wong
 * @class This class is attach to the LoadPlayer GameObject
 */ 
public class LoadPlayers : MonoBehaviour
{
	/**
	 * The Name Input Field.
	 */ 
	public InputField nameInputField;

	/**
	 * The Email Input Field.
	 */ 
	public InputField emailInputField;

	/**
	 * The Player List Text.
	 */ 
	public Text playerListText;

	/**
	 * The Submit Button.
	 */ 
	public Button submitButton;

	/**
	 * The maximum number of players.
	 */ 
	public const int NUMBER_OF_PLAYERS = 4;

	/**
	 * The maximum number of chaos players.
	 */ 
	public const int NUMBER_OF_CHAOS_PLAYERS = 1;

	/**
	 * The collection that holds players. 
	 */ 
	private static Player[] Players;

	/**
	 * Count the number of players.
	 */ 
	private static int count;

	/**
	 * Initialize the Players Object.
	 */ 
	void Start(){
		Players = new Player[NUMBER_OF_PLAYERS];
		count = 0;
	}

	/**
	 * When the Submit Button is clicked.
	 */ 
	public void submitOnClicked(){
		Debug.Log (nameInputField.text);
		Debug.Log (emailInputField.text);

		// Check to see if the name and email text field is empty
		if (nameInputField.text == null || emailInputField.text == null) {
			// TODO: Show Error Message
			return;
		}

		// Add the new Player to the Players
		Players[count] = new Player(nameInputField.text, emailInputField.text);
		count++;
		playerListText.text += " " + nameInputField.text;

		// Check to see if the number of Players is equal to NUMBER_OF_PLAYERS
		if (count != NUMBER_OF_PLAYERS) {
			// Reset the text fields.
			nameInputField.text = "Please enter a name";
			emailInputField.text = "Please enter an email";
		} else {
			// Move on to the next scene.
			Player.setPlayers(Players);
			ChangeScene.ChangeToStatic("Game");
		}
	}
}