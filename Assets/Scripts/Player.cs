﻿using UnityEngine;
using System.Collections;

/**
 * @author Anthony Wong
 * @class This class is building block of Player.
 * This class is also attach to Player Game Object.
 */ 
public class Player : MonoBehaviour {

	/**
	 * Name of the player.
	 */ 
	private string playerName;
	
	/**
	 * Email of the player.
	 */ 
	private string playerEmail;
	
	/**
	 * Player's Constructor
	 * @playerName Name of the player.
	 * @playerEmail Email of the player.
	 */ 
	public Player (string playerName, string playerEmail)
	{
		this.playerName = playerName;
		this.playerEmail = playerEmail;
	}
	
	/**
	 * @return Player's Name
	 */ 
	public string getName(){
		return this.playerName;
	}
	
	/**
 	 * @return Player's Email
	 */ 
	public string getEmail(){
		return this.playerEmail;
	}

	/**
	 * The collection that holds players. 
	 */ 
	private static Player[] Players;

	/**
	 * 
	 */ 
	void Start(){
		DontDestroyOnLoad (gameObject);
	}

	/**
	 * Set the list of collection that holds players.
	 * @Players The collection that holds players.
	 */ 
	public static void setPlayers(Player[] Players){
		Player.Players = Players;
	}

	/**
	 * @return The collection that holds players.
	 */ 
	public static Player[] getPlayers(){
		return Players;
	}
}