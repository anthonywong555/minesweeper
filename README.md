##Minesweeper
###Seminar in Computers and Society

###TODO:
- Fix the hard coded chaos player:
    - Grid.cs Line: 67.
- Increase the size of the grid:
    - To 20x20
- Improve on the LoadPeople Scene or Redesign it: 
    - Fix the nameTextField and emailTextField.
        - When someone click on the submit button, then make the text gray out.
        - Allow people to edit the email, after they submit it.
- The Law Players information is 20% inaccurate.
- Port the game to Web.
- Replace the current buttons with these: https://www.assetstore.unity3d.com/en/#!/content/25468
###Notes:
- For SMTP, I use this following code: http://www.codeproject.com/Tips/520998/Send-Email-from-Yahoo-GMail-Hotmail-Csharp
- For Minesweeper, I use this following tutorial: http://noobtuts.com/unity/2d-minesweeper-game